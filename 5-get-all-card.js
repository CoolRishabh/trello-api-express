const getBoardList = require("./3-get-board-list");
const getCard = require("./4-get-card");

function getAllCard(id) {
    return getBoardList(id).then((data) => {
        const allListPromise = data.map((list) => getCard(list.id));
        return Promise.all(allListPromise).then((allLists) => {
            const allCard = [];
            allLists.forEach((list) => {
                list.forEach((card) => {
                    allCard.push(card.name);
                });
            });
            return allCard;
        });
    });
}

// getAllCard("H5RjsxUq").then((allCard) => console.log(allCard));

module.exports = getAllCard;
