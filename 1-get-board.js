const showBoardApi = require("./API/show-board");

const idBoard = "H5RjsxUq";

function getBoard(boardId) {
    const url = showBoardApi(boardId);
    return fetch(url);
}

module.exports = getBoard;
