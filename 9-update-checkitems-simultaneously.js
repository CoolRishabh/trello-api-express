require("dotenv").config();

const apikey = process.env.API_KEY;
const token = process.env.TOKEN;

const getCheckListApi = require("./API/get-checklist-api");
const updateCheckItemApi = require("./API/update-check-item-api");

function checkAnItem(cardId, checkitemId) {
    const url = updateCheckItemApi(cardId, checkitemId, (state = "complete"));
    return fetch(url, { method: "PUT" })
        .then((response) => {
            console.log(`Response: ${response.status} ${response.statusText}`);
            return response.json();
        })
        .catch((err) => console.error(err));
}

function completeChecklist(checklistId) {
    const url = getCheckListApi(checklistId);
    return fetch(url)
        .then((response) => {
            console.log(`Response: ${response.status} ${response.statusText}`);
            return response.json();
        })
        .then((checklist) => {
            const cardId = checklist.idCard;
            const checkItemsIds = checklist.checkItems.map(
                (checkitem) => checkitem.id
            );
            return Promise.all(
                checkItemsIds.map((id) => checkAnItem(cardId, id))
            );
        })
        .catch((err) => console.error(err));
}

// completeChecklist("664b2a82508d6ed481c4e0f6")
// .then(promiseArray => {
//     console.log(promiseArray)
// })
// .catch((err) => console.error(err));

module.exports = completeChecklist;
