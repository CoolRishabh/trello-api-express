const getBoardList = require("./3-get-board-list");
const deleteList = require("./utils/delete-list");

function deleteListsSequentailly(boardId) {
    return getBoardList(boardId).then((dataList) => {
        const allList = dataList.map((list) => {
            return { id: list.id, name: list.name };
        });
        return allList;
    });
}

// deleteListsSequentailly("OXUmYQgp")
//     .then((allList) => {
//     allList.forEach((element) => {
//         deleteList(element.id)
//             .then((response) => {
//                 console.log(
//                     `Response: ${response.status} ${response.statusText}`
//                 );
//                 return response.text();
//             })
//             .then((text) => console.log(text))
//             .catch((err) => console.error(err));
//     });
// });

module.exports = deleteListsSequentailly;