const express = require("express");
const app = express();
app.use(express.json());
const port = 3001;

const getBoard = require("./1-get-board");
const createBoard = require("./2-create-board");
const getBoardList = require("./3-get-board-list");
const getCard = require("./4-get-card");
const getAllCard = require("./5-get-all-card");
const createBoardListAndCard = require("./6-create-board-list-and-card");
const deleteListsSimultaneously = require("./7-delete-lists-simultaneously");
const deleteListsSequentailly = require("./8-delete-list-sequentially");
const deleteList = require("./utils/delete-list");
const { completeChecklist, unCheckAnItem } = require("./10-update-checkitem-sequentially");

// get board by its boardId
app.get("/board/:boardId", (req, res) => {
    // const idBoard = "H5RjsxUq";
    const idBoard = req.params.boardId;
    getBoard(idBoard)
        .then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error(
                    "Response not ok with status: " + response.status
                );
            }
        })
        .then((data) => {
            console.log({ id: data.id, name: data.name });
            res.send(data);
        })
        .catch((err) => {
            console.error(err);
            res.send(err);
        });
});

// get board lists by boardId
app.get("/board/list/:boardId", (req, res) => {
    const idBoard = req.params.boardId;

    getBoardList(idBoard)
        .then((data) => {
            console.log(data);
            res.send(data);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
        });
});

// create a new board with name
app.post("/create", (req, res) => {
    const boardName = req.body.boardName;
    createBoard(boardName)
        .then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error(
                    "Response not ok with status: " + response.status
                );
            }
        })
        .then((data) => {
            console.log(data);
            res.send(data);
        })
        .catch((err) => {
            console.error(err);
            res.send(err);
        });
});

// get all cards in a list
app.get("/list/card/:listId", (req, res) => {
    const listId = req.params.listId;
    getCard(listId)
        .then((data) => {
            data.forEach((element) => {
                console.log(element.name);
            });
            res.send(data);
        })
        .catch((err) => {
            console.error(err);
            res.status(500).send("Internal Server Error");
        });
});

// list all card in a board by its boardId
app.get("/board/listAllCard/:boardId", (req, res) => {
    const boardId = req.params.boardId;
    getAllCard(boardId)
        .then((data) => {
            console.log(data);
            res.send(data);
        })
        .catch((err) => {
            console.error(err);
            res.status(500).send("Internal Server Error");
        });
});

// create a board, 3 list and cards simultaneously
app.post("/createboardListCard", (req, res) => {
    const formData = req.body;
    const boardName = formData.boardName;
    const listName = formData.listName;
    const cardName = formData.cardName;
    createBoardListAndCard(boardName, listName, cardName)
        .then((card) => {
            console.log("One card for each list created simultaneously", card);
            res.send("One card for each list created simultaneously");
        })
        .catch((err) => {
            console.error(err);
            res.status(500).send(err);
        });
});

// delete all list in a board by its boardId simultaneously
app.delete("/delete/list", (req, res) => {
    const boardId = req.body.boardId;

    deleteListsSimultaneously(boardId)
        .then((allPromise) => {
            const PromiseTexts = allPromise.map((promiseData) =>
                promiseData.text()
            );
            return Promise.all(PromiseTexts);
        })
        .then((texts) => {
            console.log("List deleted successfully");
            res.send(texts);
        })
        .catch((err) => console.error(err));
});

// delete all list in a board by its boardId sequentially
app.delete("/delete/listSeq", (req, res) => {
    const boardId = req.body.boardId;
    deleteListsSequentailly(boardId)
        .then((allList) => {
            return new Promise((resolve, reject) => {
                allList.forEach((element) => {
                    deleteList(element.id)
                        .then((response) => {
                            console.log(
                                `Response: ${response.status} ${response.statusText}`
                            );
                            return response.text();
                        })
                        .then((text) => {
                            console.log(text);
                            resolveData.push(text);
                        })
                        .catch((err) => reject(err));
                });
                resolve();
            });
        })
        .then(() => {
            res.send("Data delete sequentially");
        })
        .catch((err) => {
            console.error(err);
            res.status(500).send(err);
        });
});

// update all checkItems in checkList to completed status
app.post("/update/checkList", (req, res) => {
    // 664b2a82508d6ed481c4e0f6
    const checkListId = req.body.checkListId;
    console.log(checkListId);
    completeChecklist(checkListId)
        .then((promiseArray) => {
            console.log(promiseArray);
            res.send(promiseArray);
        })
        .catch((err) => {
            console.error(err);
            res.status(500).send(err);
        });
});

// update all checkItems in checkList to incompleted status sequentailly
app.post("/update/checkListSeq", (req, res) => {
    // 664b2a82508d6ed481c4e0f6
    const checkListId = req.body.checkListId;
    console.log(checkListId);
    completeChecklist(checkListId)
        .then((checklist) => {
            const cardId = checklist.idCard;
            return new Promise((resolve, reject) => {
                checklist.checkItems.forEach((checkItem, index) => {
                    setTimeout(() => {
                        unCheckAnItem(cardId, checkItem.id)
                            .then(() =>
                                console.log(
                                    checkItem.name + " unchecked successfully."
                                )
                            )
                            .catch((err) => console.error(err));
                    }, (index + 1) * 1000);
                });
                resolve();
            });
        })
        .then(() => {
            res.send("checkitems uncheck sequentially");
        })
        .catch((err) => {
            console.error(err);
            res.status(500).send(err);
        });
});

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
