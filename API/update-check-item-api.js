require("dotenv").config();

const apikey = process.env.API_KEY;
const token = process.env.TOKEN;

function UpdateCheckItemApi(cardId, checkItemId,state = 'complete') {
    const url = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=${state}&key=${apikey}&token=${token}`;
    return url;
}

module.exports = UpdateCheckItemApi;