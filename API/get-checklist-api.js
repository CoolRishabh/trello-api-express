require("dotenv").config();

const apikey = process.env.API_KEY;
const token = process.env.TOKEN;

function getCheckListApi(checkListId) {
    return `https://api.trello.com/1/checklists/${checkListId}?key=${apikey}&token=${token}`;
}

module.exports = getCheckListApi;
