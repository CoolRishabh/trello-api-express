const getCheckListApi = require("./API/get-checklist-api");
const updateCheckItemApi = require("./API/update-check-item-api");

function unCheckAnItem(cardId, checkitemId) {
    const url = updateCheckItemApi(cardId, checkitemId, (state = "incomplete"));
    return fetch(url, { method: "PUT" })
        .then((response) => {
            console.log(`Response: ${response.status} ${response.statusText}`);
            return response.text();
        })
        .then((text) => console.log(text))
        .catch((err) => console.error(err));
}

function completeChecklist(checklistId) {
    const url = getCheckListApi(checklistId);
    return fetch(url)
        .then((response) => {
            console.log(`Response: ${response.status} ${response.statusText}`);
            return response.json();
        })
        // .then((checklist) => {
        //     const cardId = checklist.idCard;
        //     checklist.checkItems.forEach((checkItem, index) => {
        //         setTimeout(() => {
        //             unCheckAnItem(cardId, checkItem.id)
        //                 .then(() =>
        //                     console.log(
        //                         checkItem.name + " unchecked successfully."
        //                     )
        //                 )
        //                 .catch((err) => console.error(err));
        //         }, (index + 1) * 1000);
        //     });
        // })
        .catch((err) => console.error(err));
}

// completeChecklist("664b2a82508d6ed481c4e0f6");

module.exports = {
    completeChecklist,
    unCheckAnItem
};
