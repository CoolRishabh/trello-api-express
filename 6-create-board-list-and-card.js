const createBoard = require("./2-create-board");
const createCard = require("./utils/create-card");
const createList = require("./utils/create-list");

function createBoardListAndCard(boardName='Board', listName=['list1, list2'], cardName='card1') {
    return createBoard(boardName)
        .then((response) => response.json())
        .then((data) => {
            const boardId = data.id;
            console.log("created board with id: " + boardId);
            const allList = listName.map((listName) =>
                createList(boardId, listName)
            );
            return Promise.all(allList);
        })
        .then((responseArr) => {
            return Promise.all(responseArr.map((response) => response.json()));
        })
        .then((lists) => {
            console.log("3 Lists created simultaneously");
            return Promise.all(
                lists.map((list) => {
                    return createCard(list.id, cardName);
                })
            );
        })
        .then((responseArr) => {
            return Promise.all(responseArr.map((response) => response.json()));
        })
        .catch((err) => console.error(err));
}

// createBoardListAndCard("BOARD_TEMP", ["LIST_1", "LIST_2"])
//     .then(() => {
//         console.log("One card for each list created simultaneously");
//     })
//     .catch((err) => console.error(err));

module.exports = createBoardListAndCard;