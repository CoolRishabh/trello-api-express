1. Create a function getBoard which takes the boardId as arugment and returns a promise which resolves with board data
2. Create a function createBoard which takes the boardName as argument and returns a promise which resolves with newly created board data
3. Create a function getLists which takes a boardId as argument and returns a promise which resolved with lists data
4. Create a function getCards which takes a listId as argument and returns a promise which resolves with cards data
5. Create a function getAllCards which takes a boardId as argument and which uses getCards function to fetch cards of all the lists. Do note that the cards should be fetched simultaneously from all the lists.
6. Create a new board, create 3 lists simultaneously, and a card in each list simultaneously
7. Delete all the lists created in Step 6 simultaneously
8. Delete all the lists created in Step 6 sequentially i.e. List 1 should be deleted -> then List 2 should be deleted etc.
9. Update all checkitems in a checklist to completed status simultaneously
10. Update all checkitems in a checklist to incomplete status sequentially i.e. Item 1 should be updated -> then wait for 1 second -> then Item 2 should be updated etc.

# Postman local collection
![img](./assests/local-trello-collection.png)