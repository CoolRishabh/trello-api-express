const getBoardList = require("./3-get-board-list");
const deleteList = require("./utils/delete-list");

function deleteListsSimultaneously(boardId) {
    return getBoardList(boardId).then((dataList) => {
        const allList = dataList.map((list) => {
            return { id: list.id, name: list.name };
        });
        return Promise.all(allList.map((list) => deleteList(list.id)));
    });
}

// deleteListsSimultaneously("664af642784ee20095005cc6")
    // .then((allPromise) => {
    //     const PromiseTexts = allPromise.map((promiseData) =>
    //         promiseData.text()
    //     );
    //     return Promise.all(PromiseTexts);
    // })
    // .then((texts) => {
    //     texts.forEach((text) => console.log(text));
    // })
    // .catch((err) => console.error(err));

    module.exports = deleteListsSimultaneously